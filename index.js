var decorate = require('getter-setter').browser;
var dateFormat = require('dateformat');
var obj = {};
var paymentObj = {};
var now = new Date();

exports.setObject = function () {

    var privateVar = {
        "affiliateUser": {
            "id": "",
            "name": ""
        },
        "businessUser": {
            "id": "",
            "name": ""
        },
        "actionUser": {
            "id": "",
            "name": ""
        },
        "entity": {
            "id": "",
            "name": ""
        },
        "transaction": {
            "id": "",
            "amount": ""
        },
        "type":"",
        "timestamp":"",
        "eventId": "",
        "eventType": "",
        "eventPage": "",
        "viewCount": "",
        "eventUser": "",
        "userGender": "",
        "userCity": "",
        "userState":"",
        "userCountry":"",
        "payment": "",
        "reason":"",
        "affiliate":"",
        "business":"",
        "affiliateId":"",
        "businessId":"",
        "eventUserId":"",
        "entityId":"",
        "entityUser":"",
        "transactionId":"",
        "viewCheck":"",
        "transactionCheck":"",
        "postId":"",
        "amount":  "",
        "userAge":"",
        "ipAddress":"",
        "price":"",
        "convertedOn":"",
        "affiliateEmail":"",
        "businessEmail":""
    };



    this.getDetailsWithoutTransaction = function() {
        this.seteventTime();
        // console.log(this.getEventId());
        if(this.getType() == 3) {
            if(this.getEventId() == '' && this.getEntityName == '' ) {
                return {status:0,msg:'Product Id and Product Name should be mentioned',data:''};

            } else {
                return {status:1,msg:'data generated successfully',data:this.getDetails()};
            }
        } else {
            return {status:1,msg:'data generated successfully',data:this.getDetails()};
        }

    }

    this.getDetailsWithTransaction = function(privateVar) {
        this.seteventTime();
        var transation = this.getTransaction();
        if(transation.id == '' && this.transaction.amount == '' ) {
            return {status:0,msg:'Transaction details not present',data:''};

        } else {
            return {status:1,msg:'data generated successfully',data:this.getDetails()};
        }

    }

    this.setBusinessEmail = function(value) {

        privateVar.businessEmail = value;

    }
    this.getBusinessEmail = function() {

        return privateVar.businessEmail;
    }

    this.setAffiliateEmail = function(value) {

        privateVar.affiliateEmail = value;

    }
    this.getAffiliateEmail = function() {

        return privateVar.affiliateEmail;
    }

    this.setIpAddress = function(value) {

        privateVar.ipAddress = value;

    }
    this.getIpAddress = function() {

        return privateVar.ipAddress;
    }

    this.setPrice = function(value) {

        privateVar.price = value;

    }
    this.getPrice = function() {

        return privateVar.price;
    }

    this.setConvertedOn = function(value) {

        privateVar.convertedOn = value;

    }
    this.getConvertedOn = function() {

        return privateVar.convertedOn;
    }

    this.setPostId = function(value) {

        privateVar.postId = value;

    }
    this.getPostId = function() {

        return privateVar.postId;
    }

    this.seteventId = function(value) {

        privateVar.eventId = value;

    }
    this.getEventId = function() {

        return privateVar.eventId;
    }

    this.getEntityName = function() {

        return privateVar.entity.name;
    }



    this.setEventPage = function(value) {

        privateVar.eventPage = value;

    }
    this.getEventPage = function() {

        return privateVar.eventPage;
    }

    this.setTimeStamp = function(value) {
        privateVar.timestamp = value;

    }


    this.setViewCount = function(value) {

        privateVar.viewCount = value;

    }
    this.getViewCount = function() {

        return privateVar.viewCount;
    }

    this.setEventUser = function(value) {

        privateVar.eventUser = value;

    }
    this.getEventUser = function() {

        return privateVar.eventUser;
    }

    this.setUserAge = function(value) {

        privateVar.userAge = value;

    }
    this.getUserAge = function() {

        return privateVar.userAge;
    }

    this.setUserState = function(value) {

        privateVar.userState = value;

    }
    this.getUserState = function() {

        return privateVar.userState;
    }

    this.setUserCity = function(value) {

        privateVar.userCity = value;

    }
    this.getUserCity = function() {

        return privateVar.userCity;
    }

    this.setUserCountry = function(value) {

        privateVar.userCountry = value;

    }
    this.getUserCountry = function() {

        return privateVar.userCountry;
    }

    this.setPayment = function(value) {

        privateVar.payment = value;

    }
    this.getPayment = function() {

        return privateVar.payment;
    }



    /**
     * setter /getter for gender
     */

    this.setReason = function(value) {
        privateVar.reason = value;




    }
    this.getReason = function() {
        return privateVar.get("reason");
    }

    /**
     * setter /getter for gender
     */

    this.setUserGender = function(value) {
        privateVar.userGender = value;

    }
    this.getUserGender = function() {
        return privateVar.userGender;
    }

    /**
     * setter /getter for event type
     */

    this.setType = function(value) {

        privateVar.eventType =  type[value];

    }

    this.setEventType = function(value) {
        privateVar.type = value;


    }

    this.getType = function() {
        return privateVar.eventType;
    }

    this.setAmount = function(value) {

        privateVar.amount = value;

    }
    this.getViewCheck = function() {

        return privateVar.viewCheck;
    }

    this.setViewCheck = function(value) {

        privateVar.viewCheck = value;

    }

    this.getTransactionCheck = function() {

        return privateVar.transactionCheck;
    }

    this.setTransactionCheck = function(value) {

        privateVar.transactionCheck = value;

    }

    this.getAmount = function() {

        return privateVar.amount;
    }

    /**
     * setter /getter for businessUser
     */

    this.setBusinessUser = function(value) {

        privateVar.businessUser.id =  value.id;
        privateVar.businessUser.name = value.name;

    }
    this.getBusinessUser = function() {
        return privateVar.businessUser;
    }

    /**
     * setter /getter for affiliateUser
     */

    this.setAffiliateUser = function(value) {

        privateVar.affiliateUser.id =  value.id;
        privateVar.affiliateUser.name = value.name;

    }
    this.getAffiliateUser = function() {
        return privateVar.affiliateUser;
    }

    /**
     * setter /getter for actionuser
     */

    this.setActionUser = function(value) {

        privateVar.actionUser.id =  value.id;
        privateVar.actionUser.name = value.name;

    }
    this.getActionUser = function() {
        return privateVar.actionUser;
    }

    /**
     * setter /getter for entity
     */

    this.setEntity = function(value) {

        privateVar.entity.id =  value.id;
        privateVar.entity.name = value.name;

    }
    this.getEntity = function() {
        return privateVar.entity;
    }



    /**
     * setter /getter for transaction
     */

    this.setTransaction = function(value) {

        privateVar.transaction.id =  value.id;
        privateVar.transaction.amount = value.amount;

    }
    this.getTransaction = function() {
        return privateVar.transaction;
    }

    this.getDetails = function() {
        return privateVar;
    }

    this.seteventTime = function() {
        //Date().getUTCDate()
        var getUTCTime = dateFormat(now, "isoUtcDateTime");//isoDateTime//isoUtcDateTime
        privateVar.eventTime = new Date().toISOString();

    }



}

exports.getTypes = function() {
    return type;
}

var flatJson = {
    "type":"",
    "timestamp":"",
    "eventId": "",
    "eventType": "",
    "eventPage": "",
    "viewCount": "",
    "eventUser": "",
    "userGender": "",
    "userCity": "",
    "userState":"",
    "userCountry":"",
    "payment": "",
    "reason":"",
    "affiliate":"",
    "business":"",
    "affiliateId":"",
    "businessId":"",
    "eventUserId":"",
    "productId":"",
    //"entityUser":"",
    "transactionId":"",
    "viewCheck":"",
    "transactionCheck":"",
    "userAge":"",
    "ipAddress":"",
    "price":"",
    "convertedOn":"",
    "affiliateEmail":"",
    "businessEmail":""

}

exports.getObject = function(msg) {

    var object = new this.setObject();
    //console.log(object);
    var consumerData = msg.data;
    //console.log(JSON.stringify(consumerData));
    // console.log(consumerData.affiliateUser.id);
    object.seteventId(consumerData.eventId);
    object.setAmount(consumerData.amount);
    object.setEventType(consumerData.type)  ;
    object.setEventPage(consumerData.eventPage)  ;
    object.setAffiliateUser({"id": consumerData.affiliateUser.id, "name": consumerData.affiliateUser.name});
    object.setBusinessUser({"id": consumerData.businessUser.id, "name": consumerData.businessUser.name});
    object.setActionUser({"id": consumerData.actionUser.id, "name": consumerData.actionUser.name});
    object.setTransaction({"id": consumerData.transaction.id, "amount": consumerData.transaction.name});
    object.setEntity({"id": consumerData.entity.id, "name": consumerData.entity.name});

    object.setViewCount(consumerData.viewCount) ;
    object.setUserGender(consumerData.userGender) ;
    object.setUserCity(consumerData.userCity)  ;
    object.setUserState(consumerData.userState)  ;
    object.setUserCountry(consumerData.userCountry)  ;
    object.setReason(consumerData.reason) ;

    object.setTimeStamp(consumerData.eventTime);
    object.setPostId(consumerData.postId) ;
    object.setUserAge(consumerData.userAge)  ;
    object.setViewCheck(consumerData.viewCheck)  ;
    object.setTransactionCheck(consumerData.transactionCheck)  ;
    object.seteventTime(consumerData.eventTime) ;

    object.setIpAddress(consumerData.ipAddress)  ;
    object.setPrice(consumerData.price)  ;
    object.setConvertedOn(consumerData.convertedOn) ;

    object.setAffiliateEmail(consumerData.affiliateEmail) ;
    object.setBusinessEmail(consumerData.businessEmail) ;

    return object;
}


var type = {
    "page-view":1,
    "business-follow":2,
    "product-view":3,
    "transaction-completed":4
};

var typeName = {
    1:"page-view",
    2:"business-follow",
    3:"product-view",
    4:"transaction-completed"
};


exports.getFlatEventsData = function(data) {

    flatJson.eventId = data.data.eventId;
    flatJson.eventType = data.data.type;
    flatJson.eventPage = data.data.eventPage;
    flatJson.affiliateUserId = data.data.affiliateUser.id;
    flatJson.affiliate = data.data.affiliateUser.name;
    flatJson.businessId = data.data.businessUser.id;
    flatJson.business = data.data.businessUser.name;
    flatJson.actionId = data.data.actionUser.id;
    flatJson.eventUser = data.data.actionUser.name;
    flatJson.viewCount= data.data.viewCount;
    flatJson.userGender = data.data.userGender;
    flatJson.userCity = data.data.userCity;
    flatJson.userState = data.data.userState;
    flatJson.userCountry = data.data.userCountry;
    flatJson.reason = data.data.reason;
    flatJson.productId = data.data.entity.id;
    //flatJson.entityUser = data.data.entity.name;
    flatJson.paymentId = data.data.transaction.id;
    flatJson.payment = data.data.transaction.amount;
    flatJson.timestamp = data.data.eventTime;
    flatJson.postId = data.data.postId;
    flatJson.userAge = data.data.userAge;
    flatJson.viewCheck = data.data.userAge;
    flatJson.transactionCheck = data.data.transactionCheck;
    flatJson.userAge = data.data.userAge;
    flatJson.viewCheck = data.data.userAge;
    flatJson.transactionCheck = data.data.transactionCheck;

    flatJson.price = data.data.price;
    flatJson.convertedOn = data.data.convertedOn;
    flatJson.ipAddress = data.data.ipAddress;

    flatJson.affiliateEmail = data.data.affiliateEmail;
    flatJson.businessEmail = data.data.businessEmail;

    return flatJson;
}


